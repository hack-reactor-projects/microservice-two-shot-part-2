from django.db import models
from django.urls import reverse
# Create your models here.


class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    bin_number = models.IntegerField()
    bin_size = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200, )
    model = models.CharField(max_length=200, )
    color = models.CharField(max_length=200, null=True)
    url = models.URLField()
    wardrobe = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE, null=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
