from django.contrib import admin
from .models import Shoes, BinVO

# Register your models here.


class ShoesVOAdmin(admin.ModelAdmin):
    pass


admin.site.register(Shoes)


class BinVOAdmin(admin.ModelAdmin):
    pass


admin.site.register(BinVO)
