from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
# Create your views here.


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number","bin_size","import_href",]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model", "color", "url"]


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer",
                  "model",
                  "color",
                  "url",
                  "wardrobe",
                  ]
    encoders = {"wardrobe":BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesDetailEncoder)
    else:
        content = json.loads(request.body)
        # try & except block
        try:
            href = content["wardrobe"]
            wardrobe = BinVO.objects.get(import_href=href)
            content["wardrobe"] = wardrobe
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        hat = Shoes.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "DELETE":
        count, _ = Shoes.object.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
