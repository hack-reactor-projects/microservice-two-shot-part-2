import React from "react";
import { Link } from "react-router-dom";


function HatsColumn(props) {
    console.log(props.list)
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.href} className="card mb-3 shadow">
                        <img src={hat.picture_url} className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{hat.fabric} {hat.style}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{hat.color}</h6>
                            <p>You can find this cap in {hat.location.closet_name}</p>
                        </div>
                        <div className='card-footer btn-primary'>
                            <button onClick={async () => await props.handleDelete(hat)}>Remove your hats from the wardrobe</button>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}

class HatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatColumns: [[], [], []],
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/"

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatColumns = [[], [], []];

                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i += 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse)
                    }
                }
                this.setState({ hatColumns: hatColumns});
            }
        } catch (e) {
            console.error(e);
        }
    }

    handleDelete = async (hatToDelete) => {
        const deleteUrl = `http://localhost:8090${hatToDelete.href}`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            let filteredHatColumns = [
                ...[this.state.hatColumns[0].filter(hat => hat.href !== hatToDelete.href)],
                ...[this.state.hatColumns[1].filter(hat => hat.href !== hatToDelete.href)],
                ...[this.state.hatColumns[2].filter(hat => hat.href !== hatToDelete.href)]];
            this.setState({hatColumns: filteredHatColumns})
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Wardrobify Hat Home!</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                            The spot for all of your favorite Hats!
                        </p>
                        { <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add your hat</Link>
                        </div> }
                    </div>
                </div>
                <div className="container">
                    <h2>Hats Hats Hats!</h2>
                    <div className="row">
                        {this.state.hatColumns.map((hatList, index) => {
                            return (
                                <HatsColumn handleDelete={this.handleDelete} key={index} list={hatList} />
                            );
                        })}
                    </div>
                </div>
            </>
        )
    }
}

export default HatsList
