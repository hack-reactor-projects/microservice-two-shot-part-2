import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {
  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/hats" element={<HatList />} />
            <Route path="/hats/new" element={<HatForm/>} />
          </Routes>
        </div>
      </BrowserRouter>
      <Outlet />
    </>
  );
}

export default App;
