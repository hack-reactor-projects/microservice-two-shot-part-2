import React from "react";

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            fabric: "",
            style: "",
            color: "",
            pictureUrl: "",
            location: "",
            submitted: false
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.locations;
        delete data.submitted;

        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                fabric: "",
                style: "",
                color: "",
                pictureUrl: "",
                location: "",
                submitted: true,
            };
            this.setState(cleared);
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value })
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value })
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }
    async componentDidMount() {
        const response = await fetch("http://localhost:8100/api/locations/")

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations })
        }
    }
    render() {
        let formClass = "hat-form"
        let successClass = "alert alert-success d-none mb-0"
        if(this.state.submitted === true){
            formClass = "d-none"
            successClass = "alert alert-success mb-0"
        }
        return (
            <div className="my-5" container="true">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="public/logo192.png/" alt="" />
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form className={formClass} onSubmit={this.handleSubmit} id="create-hat-form">
                                    <h1 className="card-title">Add a Hat</h1>
                                    <p className="mb-3">
                                        We need the deets!
                                    </p>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.fabric} onChange={this.handleFabricChange} required placeholder="What's the fabric?" type="text" id="fabric" name="fabric" className="form-control" />
                                                <label htmlFor="fabric">Fabric Used: </label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.style} onChange={this.handleStyleChange} required placeholder="What's the style?" type="text" id="style" name="style" className="form-control" />
                                                <label htmlFor="style">Style that Hat:</label>
                                            </div>
                                        </div>
                                        <div className='col'>
                                            <div className='form-floating mb-3'>
                                                <input value={this.state.color} onChange={this.handleColorChange} required placeholder="How 'bout the color(s)?" type="text" id="color" name="color" className='form-control' />
                                                <label htmlFor="color">Pick a Color:</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.pictureUrl} onChange={this.handlePictureUrlChange} required placeholder="Show me a pic!" type="text" id="picture_url" name="picture_url" className="form-control" />
                                                <label htmlFor="pictureUrl">Show me a Pic:</label>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <select value={this.state.location} onChange={this.handleLocationChange} name="location" id="location" className="form-select" required>
                                                <option>Where should they go?</option>
                                                {this.state.locations.map(location => {
                                                    return (
                                                        <option key={location.href} value={location.href}>
                                                            {location.closet_name}
                                                        </option>
                                                    );
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Wardrobify</button>
                                </form>
                                <div className={successClass} id="success-message">
                                    Look at that Fancy Wardrobe!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HatForm
