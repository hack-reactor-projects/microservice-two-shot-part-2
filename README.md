# Wardrobify

Team:

* Josh - Hats
* Brooke - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The hats microservice is one that will include a Model with fabric, style_name, color, picture_url, and location (based on the locationVO which will pull from the wardrobe) properties.
