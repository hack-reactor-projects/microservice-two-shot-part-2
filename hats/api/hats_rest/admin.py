from django.contrib import admin
from .models import LocationVO, Hats

# Register your models here.
class LocationVOAdmin(admin.ModelAdmin):
    pass
admin.site.register(LocationVO, LocationVOAdmin)

class HatsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Hats, HatsAdmin)
